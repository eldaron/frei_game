﻿using UnityEngine;
using System.Collections;

public class Character
{

		public string charName;
		public int id;
        public int maxMana, maxHits, mana, hits;
        public int attack, defense;

		public Character (int _id, string _name)
		{
				id = _id;
				charName = _name;
		}

        public void SetStats(int _attack, int _defense, int _mana, int _hits)
        {
            attack = _attack;
            defense = _defense;
            mana = _mana;
            maxMana = _mana;
            hits = _hits;
            maxHits = _hits;
        }
		public Character ()
		{
		
		}
}
