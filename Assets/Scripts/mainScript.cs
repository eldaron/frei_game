﻿using UnityEngine;
using System.Collections;

public class mainScript : MonoBehaviour
{
		public string login;
		public string password;
		public string mail;
		public string PHPUrl = "http://nwod.com.alias.hostingasp.ru/login2.ashx"; // адрес скрипта
		public GUISkin menuSkin;
		private bool loginCorrect = true;
		private bool isRegistering = false;
		private bool mailGood = true;
		private bool loginGood = true;
		private bool serverGood = true;
		private string errorMessage;
		public static string chosenLogin = "";
		public static User chosenUser;

		IEnumerator Login (string _login, string _pass)
		{
				Debug.Log ("Login");
				WWWForm form = new WWWForm ();
				form.AddField ("login", _login);
				form.AddField ("password", _pass);
				form.AddField ("code", "login");
				var r = new WWW (PHPUrl, form);
				yield return r;
				if (r.error != null) {
						Debug.Log ("Fail");
						Debug.Log (r.error);
						serverGood = false;
				} else {
						Debug.Log ("Success");
						string result = r.text.Split ('>') [0];
						if (result [0] == 'y') {
								Camera.main.backgroundColor = Color.black;
								chosenUser = new User ();
								chosenUser.login = _login;
								chosenUser.password = _pass;
								string id = r.text.Split ('>') [1];
                                chosenUser.id = id;//int.Parse (id);
								chosenLogin = _login;
								Application.LoadLevel ("SecondScene");
						} else 
								loginCorrect = false;
						Camera.main.backgroundColor = Color.yellow;
						Debug.Log (r.text);
				}
		
		}

		IEnumerator Register (string _login, string _pass, string _mail)
		{
				WWWForm form = new WWWForm ();
				mailGood = true;
				loginGood = true;
				form.AddField ("login", _login);
				form.AddField ("password", _pass);
				form.AddField ("mail", _mail);
				form.AddField ("code", "mail");
				var r = new WWW (PHPUrl, form);
				yield return r;
				if (r.error != null) {
						Debug.Log ("Fail");
						Debug.Log (r.error);
						serverGood = false;
				} else {
			
						Debug.Log ("Success");
						string result = r.text.Split ('<') [0];
						Debug.Log (r.text);
						switch (result [0]) {
						case 'm':
								mailGood = false;
								break;
						case 'l':
								loginGood = false;
								break;
						case 'y':
								isRegistering = !isRegistering;
								break;
						default:
								break;
						}
				}
		
		}

		void Awake ()
		{
				Screen.SetResolution (720, 1280, true);
		}

		void Update ()
		{
				if (Input.GetKeyDown (KeyCode.Escape)) 
						Application.Quit (); 
		}

		void OnGUI ()
		{
				float scale = 1.1f;
				GUI.matrix = Matrix4x4.Scale (new Vector3 (scale, scale, scale));
				GUI.skin.textField.fontSize = 30;
				GUI.skin = menuSkin;
				login = GUI.TextField (new Rect (Screen.width / 2 - 150, 130, 300, 40), login, 25);
				password = GUI.PasswordField (new Rect (Screen.width / 2 - 150, 220, 300, 40), password, '*', 25);


				if (!serverGood) {
						errorMessage = "Сервер не отвечает!";
				} else if (!loginGood) {
						errorMessage = "Логин занят";
			
				} else if (!loginCorrect) {
						errorMessage = "Неправильный логин или пароль";
			
				} else if (!mailGood) {
						errorMessage = "Почта занята";
			
				}

				if ((!serverGood) || (!loginGood) || (!mailGood) || (!loginCorrect)) {
						GUI.color = Color.red;
						GUI.Box (new Rect (150, 150, Screen.width - 300, Screen.height - 300), errorMessage);
						if (GUI.Button (new Rect (Screen.width / 2 - 80, 200, 160, 50), "OK")) {
								mailGood = true;
								loginGood = true;
								serverGood = true;
								loginCorrect = true;
						}
				} else {
						GUI.Label (new Rect (Screen.width / 2 - 150, 100, 300, 40), "Логин");
						login = GUI.TextField (new Rect (Screen.width / 2 - 150, 130, 300, 40), login, 25);
						GUI.color = Color.white;
						GUI.Label (new Rect (Screen.width / 2 - 150, 180, 300, 40), "Пароль");
						password = GUI.PasswordField (new Rect (Screen.width / 2 - 150, 220, 300, 40), password, '*', 25);
						if (isRegistering) {            
								GUI.Label (new Rect (Screen.width / 2 - 150, 260, 300, 40), "Почта");
								mail = GUI.TextField (new Rect (Screen.width / 2 - 150, 300, 300, 40), mail, 25);
								GUI.color = Color.white;
								if (GUI.Button (new Rect (Screen.width / 2 - 100, 400, 160, 50), "Зарегистрироваться")) {
										StartCoroutine (Register (login, password, mail));
								}
						} else {
			
								if (GUI.Button (new Rect (Screen.width / 2 - 80, 300, 160, 50), "Войти")) {
										StartCoroutine (Login (login, password));
										Debug.Log ("Кнопка уже нажата");
								}
								if (GUI.Button (new Rect (Screen.width / 2 - 100, 400, 200, 50), "Регистрация")) {
										isRegistering = !isRegistering;
								}
						}
				}

		}
}
