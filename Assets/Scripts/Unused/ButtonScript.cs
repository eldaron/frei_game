﻿using UnityEngine;
using System.Collections;

public class ButtonScript : MonoBehaviour {

	// Use this for initialization
	void OnMouseDown()
	{
		GameObject.FindGameObjectWithTag ("Player").transform.Translate (1, 0, 0);
		Camera.main.backgroundColor = Color.cyan;
	}
	void OnMouseUp()
	{
		GameObject.FindGameObjectWithTag ("Player").transform.Rotate (0.1f, 0, 0);
		Camera.main.backgroundColor = Color.green;
	}
}
