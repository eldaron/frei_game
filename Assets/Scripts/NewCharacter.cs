﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Parse;

public class NewCharacter : MonoBehaviour {
	public Text cname,mana,hits,attack,defense;
	public string PHPUrl = "http://nwod.com.alias.hostingasp.ru/addchar.ashx";
    Showmessage showmessage;
	// Use this for initialization
	void Awake () {
        showmessage = GetComponent<Showmessage>();
	}

	IEnumerator AddCharacter(string _name,string _attack,string _defense,string _mana,string _hits)
	{
        var query = ParseObject.GetQuery("Characters")
            .WhereEqualTo("name",_name);
        var task = query.FirstOrDefaultAsync();
        while (!task.IsCompleted)
            yield return null;
        if (task.Result != null)
        {
            showmessage.ShowMessage("Персонаж с таким именем уже существует");
        }
        else
        {
            ParseObject character = new ParseObject("Characters");
            character["name"] = _name;
            character["attack"] = int.Parse(_attack);
            character["defense"] = int.Parse(_defense);
            character["mana"] = int.Parse(_mana);
            character["hits"] = int.Parse(_hits);
            character["userID"] = GameController.GetInstance().chosenUserParse.ObjectId;
            var saveTask = character.SaveAsync();
            while (!saveTask.IsCompleted)
                yield return null;
            List<ParseObject> pObjs = new List<ParseObject>();
            pObjs.Add(character);
            pObjs.Add(character);
            GameController.GetInstance().chosenUserParse["charactersList"] = pObjs;
            saveTask =GameController.GetInstance().chosenUserParse.SaveAsync();
            while (!saveTask.IsCompleted)
                yield return null;
            Application.LoadLevel("SecondScene");
        }
        //Debug.Log (_name);
        //WWWForm form = new WWWForm ();
        //form.AddField ("login", GameController.GetInstance().chosenUser.id);
        //form.AddField ("name", _name);
        //form.AddField ("attack", _attack);
        //form.AddField ("defense", _defense);
        //form.AddField ("mana", _mana);
        //form.AddField ("hits", _hits);
        //var r = new WWW (PHPUrl, form);
        //yield return r;
        //if (r.error != null) {
        //    Debug.Log ("Fail");
        //    Debug.Log (r.error);
        //} else {
        //    Debug.Log ("Success");
        //    string result = r.text.Split ('<') [0];
        //    if (result [0] == 'y') {
        //        Application.LoadLevel ("SecondScene");
        //    } else 
        //        Debug.Log (r.text);
			
        //}
    }
    public void Submit()
    {
        StartCoroutine(AddCharacter(cname.text, attack.text, defense.text, mana.text, hits.text)); ;
    }
    // Update is called once per frame
   /* void OnGUI() {
        GUI.skin.textField.fontSize = 30;
		GUI.skin.label.fontSize = 30;
		GUI.Label (new Rect (Screen.width / 2 - labelWidth - margin, margin, labelWidth, labelHeight), "Имя");
		cname = GUI.TextField (new Rect (Screen.width / 2 +margin, margin, textFileldWidth, textFileldHeight),cname,25);

		GUI.Label (new Rect (margin, margin*2+labelHeight, labelWidth, labelHeight), "Атака");
		attack = GUI.TextField (new Rect (margin*2+labelWidth, margin*2+labelHeight, textFileldWidth, textFileldHeight),attack,25);
		GUI.Label (new Rect (margin, margin*3+labelHeight*2, labelWidth, labelHeight), "Защита");
		defense = GUI.TextField (new Rect (margin*2+labelWidth, margin*3+labelHeight*2, textFileldWidth, textFileldHeight),defense,25);

		
		GUI.Label (new Rect (Screen.width/2+ margin, margin*2+labelHeight, labelWidth, labelHeight), "Мана");
		mana = GUI.TextField (new Rect (Screen.width/2+margin*2+labelWidth, margin*2+labelHeight, textFileldWidth, textFileldHeight),mana,25);
		GUI.Label (new Rect (Screen.width/2 +margin, margin*3+labelHeight*2, labelWidth, labelHeight), "Хиты");
		hits = GUI.TextField (new Rect (Screen.width/2+margin*2+labelWidth, margin*3+labelHeight*2, textFileldWidth, textFileldHeight),hits,25);
	
		if (GUI.Button (new Rect (Screen.width / 2 - buttonWidth / 2, margin * 4 + labelHeight * 3, buttonWidth, buttonHeight), "Добавить")) {
			StartCoroutine (AddCharacter(cname,attack,defense,mana,hits));
        }
    }*/
}
