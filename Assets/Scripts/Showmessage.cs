﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Showmessage : MonoBehaviour {
    public GameObject panel;
    public Text messText;
    public Button buttonOk;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
       
	}
    public void ShowMessage(string message)
    {
       // GameController.GetInstance().SetActiveObjects("UI", false);
        panel.SetActive(true);
        messText.text = message;   
    }
    public void Clik_OK()
    {
        panel.SetActive(false);
       // GameController.GetInstance().SetActiveObjects("UI", true);
    }
}
