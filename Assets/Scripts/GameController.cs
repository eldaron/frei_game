﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Parse;

public class GameController : MonoBehaviour {

    static GameController gameController;
    private List<Character> interactions;
    public ParseObject chosenUserParse;
    public User chosenUser;
    public Character chosenCharacter;
    public int choseCharacterId;

	void Awake () {

        if (gameController == null)
        {
            DontDestroyOnLoad(gameObject);
            gameController = this;
        }
        else
        {
            Destroy(gameObject);
        }
        interactions = new List<Character>();
	}
    public static GameController GetInstance()
    {
        return gameController;
    }

    public void CreateUser(string _login, string _pass, string _id)
    {

        chosenUser = new User();
        chosenUser.login = _login;
        chosenUser.password = _pass;
        chosenUser.id = _id;//int.Parse(rtext.Split('>')[1]);
    }
    public bool IsInInteractions(int id)
    {
        bool result = false;
        var query = from c in interactions where c.id == id
                    select c.charName;
        if (query.FirstOrDefault() != null)
            result = true;
        return result;
    }

    public void AddInteraction(Character character)
    {
        if (!IsInInteractions(character.id))
            interactions.Add(character);
    }
    public void SetActiveObjects(string tag,bool setActive)
    {
       GameObject[] objs = GameObject.FindGameObjectsWithTag(tag);
       foreach (GameObject obj in objs)
       {
           obj.SetActive(setActive);

       }
    }
	
}
