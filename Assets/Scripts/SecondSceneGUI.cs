﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Parse;

public class SecondSceneGUI : MonoBehaviour
{
    public Button button;
    public Canvas canvas;
    string[] characterArray = null;
    private bool isAnimating = false;
    private List<Button> buttons;
    public static string chosenCharacter = "";
    public string PHPUrl = "http://nwod.com.alias.hostingasp.ru/Main.ashx"; // адрес скрипта;
    // Use this for initialization
    IEnumerator GetAllCharactersOfUser(string login)
    {
        //var fetchTask = GameController.GetInstance().chosenUserParse.FetchAsync();
       // while (!fetchTask.IsCompleted)
        //    yield return null;
        List<object> pObjs = GameController.GetInstance().chosenUserParse["charactersList"] as List<object>;
        //Debug.Log(GameController.GetInstance().chosenUserParse.Get<List<ParseObject>>("charactersList"));
        if (pObjs != null)
        {
            buttons = new List<Button>();
            for (int i = 0; i <pObjs.Count; i++)
            {
                Button b = (Button)Instantiate(button);
                var fetchTask = ((ParseObject)pObjs[i]).FetchAsync();
                while (!fetchTask.IsCompleted)
                    yield return null;
                b.gameObject.GetComponentInChildren<Text>().text = ((ParseObject)pObjs[i])["name"] as string;
                b.transform.SetParent(canvas.transform, false);
                //b.transform.Translate(new Vector3(0, 20-i*10, 0));
                //string id = pObjs[i].ObjectId;
               // b.onClick.AddListener(delegate() { Char_Click(id); });
                buttons.Add(b);
            }
            isAnimating = true;
        }
        yield return null;
        /*WWWForm form = new WWWForm();
        form.AddField("login", GameController.GetInstance().chosenUser.id);
        form.AddField("code", "characters");
        var r = new WWW(PHPUrl, form);
        yield return r;
        Debug.Log(r.text);
        if (r.error != null)
        {
            Debug.Log("Fail");
            Debug.Log(r.error);
        }
        else
        {
            if (r.text != null)
            {
                FillCharactersOnCanvas(r.text);
            }
        }*/
    }

    private void FillCharactersOnCanvas(string rtext)
    {
        if (rtext != "")
        {
            string characters = rtext.Substring(0, rtext.Length - 1);
            characterArray = characters.Split(';');
            buttons = new List<Button>();
            for (int i = 0; i < characterArray.Length; i++)
            {
                Button b = (Button)Instantiate(button);
                b.gameObject.GetComponentInChildren<Text>().text = characterArray[i].Split(':')[0];
                b.transform.SetParent(canvas.transform,false);
                //b.transform.Translate(new Vector3(0, 20-i*10, 0));
                string id = characterArray[i].Split(':')[1];
                b.onClick.AddListener(delegate() { Char_Click(id); });
                buttons.Add(b);
            }
            isAnimating = true;
        }
    }
    void Awake()
    {
        StartCoroutine(GetAllCharactersOfUser(GameController.GetInstance().chosenUser.login));
    }

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
        if (isAnimating)
        {
            for(int i=0;i<buttons.Count;i++)
            {
                buttons[i].GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(buttons[i].GetComponent<RectTransform>().anchoredPosition, new Vector2(0, 140 - i * 180), 5.5f*Time.deltaTime);
                //buttons[i].GetComponent<RectTransform>().Translate(0, 1, 0);
            }
        }
    }

    //void OnGUI()
    //{
    //    GUI.skin = menuSkin;
    //    if (charactersReturned)
    //    {

    //        if ((characters != "") && (!madeSplit))
    //        {
    //            Debug.Log("попал в ongui " + characters);
    //            characters = characters.Substring(0, characters.Length - 1);
    //            characterArray = characters.Split(';');
    //            numOfReturned = characterArray.Length;

    //        }
    //        madeSplit = true;
    //        for (int i = 0; i < numOfReturned; i++)
    //        {
    //            if (GUI.Button(new Rect(Screen.width / 2 - buttonWidth / 2, margin * (i + 1) + buttonHeight * i, buttonWidth, buttonHeight), characterArray[i]))
    //            {
    //                chosenCharacter = characterArray[i];
    //                Application.LoadLevel("FightScene");
    //            }
    //        }
    //        if (numOfReturned < 3)
    //        {
    //            if (GUI.Button(new Rect(Screen.width / 2 - buttonWidth / 2, margin * (numOfReturned + 1) + buttonHeight * numOfReturned, buttonWidth, buttonHeight), "Новый персонаж"))
    //            {
    //                Application.LoadLevel("NewCharacterScene");
    //            }
    //        }
    //    }
    //}
    public void NewChar_Click()
    {
        Application.LoadLevel("NewCharacterScene");
    }
    public void Char_Click(string char_id)
    {
        Debug.Log(char_id);
        GameController.GetInstance().choseCharacterId = int.Parse(char_id);
        Application.LoadLevel("FightScene");
    }
}
