﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class FightScript : MonoBehaviour
{
		
		string victim = "", log = "Начало лога";
		private Ping ping;
		public List<Character> allChars;
		double timeInterval = 0;
		public Popup.ListCallBack myDelegate;
		public GUISkin generalSkin;
		public string PHPUrl = "http://nwod.com.alias.hostingasp.ru/fight.ashx";
        public string PHPUrlMain = "http://nwod.com.alias.hostingasp.ru/main.ashx";
        public StyledComboBox comboBox;
        // Use this for initialization

		

		IEnumerator GetChars ()
		{
		
				Debug.Log ("GetChar");
				WWWForm form = new WWWForm ();
				Debug.Log ("во время гетчар");
				form.AddField ("code", "getchars");
				var r = new WWW (PHPUrlMain, form);
				yield return r;
				if (r.error != null) {
						Debug.Log ("Fail");
						Debug.Log (r.error);
				} else {
						if (r.text != null) {
								string[] strs = r.text.Split (';');
								allChars = new List<Character> ();
                                foreach (string item in strs)
                                {
                                    if ((item != null) && (item != ""))
                                    {
                                        Character character = new Character(int.Parse(item.Split(':')[0]), item.Split(':')[1]);
                                        allChars.Add(character);
                                    }
                                    
                                }
                                var query = from c in allChars
                                            select c.charName;
                                comboBox.AddItems(query.Take(5).ToArray());
						}
				}
			  
		}

        Character ChosenVictim()
        {
            return allChars[comboBox.SelectedIndex];
        }

        //IEnumerator CheckInteraction (string _actor, string _victim, string _message)
        //{
        //        Debug.Log ("CheckInteraction " + _actor + SecondSceneGUI.chosenCharacter);
        //        WWWForm form = new WWWForm ();
        //        form.AddField ("actor", _actor);
        //        form.AddField ("victim", _victim);
        //        form.AddField ("code", "inter");
        //        var r = new WWW (PHPUrl, form);
        //        yield return r;
        //        Debug.Log (r.text);
        //        if (r.error != null) {
        //                Debug.Log ("Fail");
        //                Debug.Log (r.error);
        //        } else {
        //                Debug.Log ("Success");
        //                string result = r.text.Split ('>') [0];
        //                if (result [0] == 'y') {
        //                        log += "\r\nПошла атака '"+_message+"' на "+_victim+"!";
        //                        StartCoroutine (Attack (_actor, _victim, _message));
        //                } else {
        //                        log += "\r\nПингую " + _victim;
        //                        StartCoroutine (ping.PingCall (GetCharID (SecondSceneGUI.chosenCharacter), GetCharID (victim), "Я тебя вижу, ты меня видишь.", 0, -1, -1));
        //                }
        //        }
        //}

		IEnumerator Scan (int _actor_id)
		{
				Debug.Log ("Scan " + _actor_id.ToString());
				WWWForm form = new WWWForm ();
				form.AddField ("actor", _actor_id.ToString());
				form.AddField ("code", "scan");
				var r = new WWW (PHPUrlMain, form);
				yield return r;
				Debug.Log (r.text);
				if (r.error != null) {
						Debug.Log ("Fail");
						Debug.Log (r.error);
				} else {
						string [] strs = r.text.Split ('&');
						for (int i=0; i<strs.Length; i++) {
								if ((strs [i] != null) && (strs [i] != "")) {
										if (strs [i].Split (';') [0] == "Ping") {
												ping.fromWhomName = strs [i].Split (';') [1].Split (':') [1];
												ping.idEvent = int.Parse (strs [i].Split (';') [3].Split ('=') [1]);
												ping.fromWhomId = int.Parse (ping.fromWhomName.Split ('(') [1].Split (')') [0]);
												ping.message = strs [i].Split (';') [2].Split (':') [1];
												ping.Activate ();
												log += "\r\nПинг:" + strs [i].Split (';') [2].Split (':') [1] + "  от " + ping.fromWhomName;
												Debug.Log (log);
										}
										if (strs [i].Split (';') [0] == "NoPing") {
												log += "\r\nХуй ты до него допинговался:" + strs [i].Split (';') [1];
										}
										if (strs [i].Split (';') [0] == "BeginFight") {
												log += "\r\nВойна началася!";
										}

								}
						}

				}
		}
        public IEnumerator PingCall(int _actor_id, int _victim_id, string _message/*, int _stage, int _id, int _result*/)
        {
            WWWForm form = new WWWForm();
            form.AddField("code", "pingbegin");
            form.AddField("actor", _actor_id.ToString());
            form.AddField("victim", _victim_id.ToString());
            form.AddField("message", _message);
            //form.AddField("stage", _stage);
            //form.AddField("id", _id);
            //form.AddField("result", _result);
            var r = new WWW(PHPUrlMain, form);
            yield return r;
            if (r.error != null)
            {
                Debug.Log("Fail");
                Debug.Log(r.error);
            }
            else
            {
                Debug.Log("Success");
                string result = r.text.Split('<')[0];
                if (result[0] == 'y')
                {
                    Debug.Log("Запрос прошел и записан в базу");
                }
                else
                {
                    Debug.Log("Запрос добавить в базу не удалось");
                }
            }

        }

		
		IEnumerator Attack (int _actor_id, int _victim_id, string _message)
		{
				Debug.Log ("Attack");
				WWWForm form = new WWWForm ();
				form.AddField ("actor", _actor_id.ToString());
				form.AddField ("victim", _victim_id.ToString());
				form.AddField ("message", _message);
				form.AddField ("code", "attack");
				var r = new WWW (PHPUrlMain, form);
				yield return r;
				if (r.error != null) {
						Debug.Log ("Fail");
						Debug.Log (r.error);
				} else {
						Debug.Log ("Success");
						string result = r.text.Split ('<') [0];
						if (result [0] == 'y') {
								Debug.Log ("Атака послана!");
						} else {
								Debug.Log ("Что-то пошло не так...");
						}
				}
		
		}

		public string GetCharID (string charname)
		{
				string str = "";
				for (int i=0; i<allChars.Count; i++)
						if (charname == allChars [i].charName)
								str = allChars [i].id.ToString ();
				return str;
		}

		void Awake ()
		{
				StartCoroutine (GetChars ());
				//myDelegate = new Popup.ListCallBack (this.FunctionToCall);
				
				// Make a GUIStyle that has a solid white hover/onHover background to indicate highlighted items
				//listStyle = new GUIStyle ();
				//listStyle.normal.textColor = Color.white;
				//Texture2D tex = new Texture2D (2, 2);
				//Color[] colors = new Color[4];
                //for (int i=0; i<4; i++)
                //        colors [i] = Color.white;
                //tex.SetPixels (colors);
                //tex.Apply ();
                //listStyle.hover.background = tex;
                //listStyle.onHover.background = tex;
                //listStyle.padding.left = listStyle.padding.right = listStyle.padding.top = listStyle.padding.bottom = 4;
				ping = GameObject.FindGameObjectWithTag ("EventManager").GetComponent<Ping> ();
				
		}

		void Update ()
		{
				timeInterval += Time.deltaTime;
				if (timeInterval > 10) {
						StartCoroutine (Scan (GameController.GetInstance().choseCharacterId));
						timeInterval = 0;
				}
				if (Input.GetKeyDown (KeyCode.Escape)) 
						Application.Quit (); 
		}

        public void Attack_Click(int num)
        {
            if (GameController.GetInstance().IsInInteractions(ChosenVictim().id))
            {
                StartCoroutine(Attack(GameController.GetInstance().choseCharacterId,ChosenVictim().id,"Я тебя вижу, ты меня видишь."));
            }
            else
                StartCoroutine(PingCall(GameController.GetInstance().choseCharacterId, ChosenVictim().id, "Я тебя вижу, ты меня видишь."));

        }
		void OnGUI ()
		{

                //if ((getAllchars) && (!fillchars))
                //        FillChars ();
                ////GUI.skin.textField.fontSize = 30;
                ////GUI.skin.label.fontSize = 30;
                //GUI.skin = generalSkin;
                //GUI.Label (new Rect (margin, margin, labelWidth, comboBoxHeight), "Жертва");
                //victim = GUI.TextField (new Rect (margin * 2 + labelWidth, margin, comboBoxWidth, comboBoxHeight), victim, 25);
                //if (Popup.List (new Rect (margin * 3 + labelWidth + comboBoxWidth, margin, 150, comboBoxHeight), ref showList, ref listEntry, new GUIContent ("Выбор"), list, listStyle, myDelegate)) {
                //        picked = true;
                //}
                //if (picked) {
                //        victim = list [listEntry];
                //        //GUI.Label (new Rect (50, 60, 400, comboBoxHeight), "You picked " + list [listEntry] + "!");
                //}


                //if (GUI.Button (new Rect (Screen.width / 4 - buttonWidth / 2, margin * 3 + 2 * comboBoxHeight, buttonWidth, buttonHeight), "Пыщ!")) {
                //        StartCoroutine (CheckInteraction (GetCharID (SecondSceneGUI.chosenCharacter), GetCharID (victim),"Пыщ"));
                //        //StartCoroutine (ping.PingCall (GetCharID (SecondSceneGUI.chosenCharacter), GetCharID (victim), "Я тебя вижу, ты меня видишь.", 0, -1, -1));
                //}


                //if (GUI.Button (new Rect (Screen.width / 4 * 3 - buttonWidth / 2, margin * 3 + 2 * comboBoxHeight, buttonWidth, buttonHeight), "Дынц!")) {
                //        StartCoroutine (CheckInteraction (GetCharID (SecondSceneGUI.chosenCharacter), GetCharID (victim),"Дынц"));
                //        //StartCoroutine (ping.PingCall (GetCharID (SecondSceneGUI.chosenCharacter), GetCharID (victim), "Я тебя вижу, ты меня видишь.", 0, -1, -1));
                //}
                //log = GUI.TextArea (new Rect (margin, Screen.height / 2 + margin * 2, 350, Screen.height / 2 - margin * 3), log);

				

		}
	    
		
	
}
