﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Parse;

public class LoginScript : MonoBehaviour
{
    public string PHPUrl = "http://nwod.com.alias.hostingasp.ru/main.ashx"; // адрес скрипта
    public Text loginText;
    public Text passText;
    public Text mailText;
    public GameObject mailLabel;
    public GameObject inputMail;
    public Button enterButton;
    public Button regButton;
    public Button regDoneButton;

    Showmessage showmessage;

    void Awake()
    {
        showmessage = GetComponent<Showmessage>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }
    /// <summary>
    /// Процедура вызова веб-сервиса для регистрации
    /// </summary>
    /// <param name="_login">логин</param>
    /// <param name="_pass">пароль</param>
    /// <param name="_mail">почта</param>
    /// <returns></returns>
    IEnumerator Register(string _login, string _pass, string _mail)
    {
        var query = ParseObject.GetQuery("Users").WhereEqualTo("login", _login);
        var task = query.FirstOrDefaultAsync();
        while (!task.IsCompleted)
            yield return null;
        if (task.Result != null)
            showmessage.ShowMessage("Такой логин уже используется!");
        else
        {
            query = ParseObject.GetQuery("Users").WhereEqualTo("mail", _mail);
            task = query.FirstOrDefaultAsync();
            while (!task.IsCompleted)
                yield return null;
            if (task.Result != null)
                showmessage.ShowMessage("Такая почта уже зарегистрирована!");
            else
            {
                ParseObject user = new ParseObject("Users");
                user["login"] = loginText.text;
                user["password"] = passText.text;
                user["mail"] = mailText.text;
                var saveTask = user.SaveAsync();
                while (!saveTask.IsCompleted)
                    yield return null;
                ChangeToLogin();
            }
        }
    //    WWWForm form = new WWWForm();
     
    //    form.AddField("login", _login);
    //    form.AddField("password", _pass);
    //    form.AddField("mail", _mail);
    //    form.AddField("code", "mail");
    //    var r = new WWW(PHPUrl, form);
    //    yield return r;
    //    if (r.error != null)
    //    {
    //        Debug.Log(r.error);
    //        showmessage.ShowMessage("Сервер не отвечает!");
    //    }
    //    else
    //    {

    //        Debug.Log("Success");
    //        string result = r.text.Split('<')[0];
    //        Debug.Log(r.text);
    //        switch (result[0])
    //        {
    //            case 'm':
    //                showmessage.ShowMessage("Такая почта уже зарегестрирована!");
    //                break;
    //            case 'l':
    //                showmessage.ShowMessage("Такой логин уже используется!");
    //                break;
    //            case 'y':
    //                ChangeToLogin();
    //                break;
    //            default:
    //                break;
    //        }
    //    }

    }
    /// <summary>
    /// Процедура вызова веб-сервиса для логина
    /// </summary>
    /// <param name="_login">логин</param>
    /// <param name="_pass">пароль</param>
    /// <returns></returns>
    IEnumerator Login(string _login, string _pass)
    {

        var query = ParseObject.GetQuery("Users")
            .WhereEqualTo("login", loginText.text)
            .WhereEqualTo("password", passText.text);
        var task = query.FirstOrDefaultAsync();
        while (!task.IsCompleted)
            yield return null;

        if (task.Result != null)
        {
            ParseObject user = task.Result;
            GameController.GetInstance().chosenUserParse = user;
            GameController.GetInstance().CreateUser(loginText.text, passText.text, user.ObjectId);
            Application.LoadLevel("SecondScene");
        }
        else
            showmessage.ShowMessage("Неправильный логин или пароль!");

       /* Debug.Log("Login");
        WWWForm form = new WWWForm();
        form.AddField("login", _login);
        form.AddField("password", _pass);
        form.AddField("code", "login");
        var r = new WWW(PHPUrl, form);
        yield return r;
        if (r.error != null)
        {
            Debug.Log("Fail");
            Debug.Log(r.error);
            Debug.Log(PHPUrl);
            showmessage.ShowMessage("Сервер не отвечает!");
        }
        else
        {
            Debug.Log("Success");
            string result = r.text.Split('>')[0];
            if (result[0] == 'y')
            {
                GameController.GetInstance().CreateUser(_login, _pass, r.text);
                Application.LoadLevel("SecondScene");
            }
            else
            {
                showmessage.ShowMessage("Неправильный логин или пароль!");
            }
            Debug.Log(r.text);
        }*/

    }

    /// <summary>
    /// Клик на логин
    /// </summary>
    public void Login_Click()
    {
        StartCoroutine(Login(loginText.text, passText.text)); ;
        
    }

    /// <summary>
    /// Клик по кнопке смены окна на регистрационное
    /// </summary>
    public void Registration_Click()
    {
        ChangeToRegister();
    }

    /// <summary>
    /// Клик по кнопке регистрируещей новый логин
    /// </summary>
    public void RegistrationDone_Click()
    {
        //ParseObject user = new ParseObject("Users");
        //user["login"] = loginText.text;
        //user["password"] = passText.text;
        //user["mail"] = mailText.text;
        //var saveTask = user.SaveAsync();
        StartCoroutine(Register(loginText.text, passText.text,mailText.text)); 
    }

    /// <summary>
    /// Меняет интерфейс на окно регистрации
    /// </summary>
    void ChangeToRegister()
    {
        mailLabel.SetActive(true);
        inputMail.SetActive(true);
        enterButton.gameObject.SetActive(false);
        regButton.gameObject.SetActive(false);
        regDoneButton.gameObject.SetActive(true);
    }
    /// <summary>
    /// Меняет интерфейс на окно Логина
    /// </summary>
    void ChangeToLogin()
    {
        mailLabel.SetActive(false);
        inputMail.SetActive(false);
        enterButton.gameObject.SetActive(true);
        regButton.gameObject.SetActive(true);
        regDoneButton.gameObject.SetActive(false);
    }
}
